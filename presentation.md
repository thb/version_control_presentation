---
author: 'Robert Beilich'
title: 'Version Control - Git'
colorlinks: true
...

# What it is

- Management
- Persistence

.. of changes on source (code)

# Why we need it

- "turning back time"
- documentation of change
- documentation of development

# The Basics
> "I'm an egotistical bastard, and I name all my projects after myself. First 'Linux', now 'git'." - *Linus Torvald*

## Version control software - Git

* developed by Linus Torvald
* initial release April 2005
* open source **!**

# The Basics
## Getting preexisting source

* `git clone <address>`{.bash} - initialize source got from somewhere else

## Initialize new version controlled source

* `git init`{.bash} - that's it! (for now)

# The Basics
## Working with a source

* `git add <files>`{.bash} - add files to the index, to tell git what should get version controlled
* `git status`{.bash} - see what's up in the source (regarding the version control)
* `git commit`{.bash} - add a new "version" of changes to the history
    * every commit has at least one parent (except the first one)
* `git log`{.bash} - see the history of commits

# The Basics
![](./graphics/git_log.png){height=90%}

# The Basics
## Working with others on a source

* everything previously mentioned applies
* `git pull`{.bash} - update your working copy with changes from a remote
    * can result in merge conflicts (more on that later)
* `git push`{.bash} - push local changes to remote (if there is one defined e.g. if cloned)
    * `git remote add <name> <address>`{.bash} - add a new remote to push to

# The Basics
## Branching

Rationale: don't mess with the main line of the source but still be able to contribute

* `git checkout <branch>` - switch to a branch
* `git checkout -b <branch>` - create branch and switch to it
  * `git branch -c <branch>` - add a new branch without switching

![branching](./graphics/branching.png){height=50%}

# Advanced
## "Diffing"

* `git diff`{.bash} - show differences between commits/files/branches/etc

~~~diff
diff --git a/HelloWorld.java b/HelloWorld.java
index f9d2a37..c8b03c6 100644
--- a/HelloWorld.java
+++ b/HelloWorld.java
@@ -1,5 +1,5 @@
 public class HelloWorld {
   public static void main(String[] args) {
-    System.out.println("Hello World!");
+    System.out.println("Hello guys!");
   }
 }
~~~

# Advanced
## Merging

* `git merge`{.bash} - integrate changes from one branch into another
    * can result in merge conflicts
    * creates a merge commit (with two parents)

![merging](./graphics/merge.png)

# Advanced
## Merge conflicts

occur if anything that got modified on both sides and the conflict can't be resolved automatically

* the merge stops and you have to resolve the conflicts
    * in the mentioned files are "diff-like" additions and you have to decide which one to keep
    * proceed as usual -> `git add` and `git commit` (maybe add a bit of information to the merge commit)

# Nice to know
## GUIs
e.g. Sourcetree
![](https://www.sourcetreeapp.com/dam/jcr:580c367b-c240-453d-aa18-c7ced44324f9/hero-mac-screenshot.png)

# Nice to know
## How to write a good commit message

* short capitalized summary
* summary in imperative (think like: "If this commit is applied it will ...")
* explanation in body, if necessary (separated from summary with a newline)

(refer to <https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html>)

# Nice to know
## Rebasing
* first of all: *NEVER* rebase a public branch!
  * only change the history if you are the only one affected (or if you hate all the others)
![](http://justinhileman.info/article/changing-history/pee-two.png)

# Nice to know
## Rebasing
* another form of integrating changes
    * "rewrites" history to append current changes *after* the ones to rebase on
    * can produce conflicts (refer to merge conflicts)
* results in "cleaner" history opposed to merge
    * changes are as close to the point they are refering to
        * no heavy merge commit including all changes
    * contra: real "history" of code production gets obfuscated a bit
        * reordering of commits in semantic order instead of chronological order
        * chronological order can still get retrieved with a bit of work
    * follow the guideline your company uses!
        * it's kind of a war between merge and rebase
* **[flow chart →](http://justinhileman.info/article/git-pretty/)**

# Nice to know
## Rebasing
![](./graphics/rebase.png)\ 

# Nice to know
## Feature based development
* repository layout
  * one master branch for production
    * *no* commits, only merges
  * (mostly) one dev branch into which the feature/ branches get merged
    * *no* commits, only merges
  * several feature/ branches where the commits happen
    * (atleast) before merging into dev rebase onto dev
    * merge into master

# Nice to know
## Feature based development
![](https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work/image-1-big.jpeg){height=80%}

# Further readings
* this presentation
    * source repo: <https://gitlab.beilich.de/thb/version_control_presentation>
    * latest pdf: <https://gitlab.beilich.de/thb/version_control_presentation/builds/artifacts/master/download?job=build> (gets removed 6 months after creation)
* <http://justinhileman.info/article/changing-history/>
* the links mentioned in the corresponding sections

# Sources
## Graphics
* <https://www.atlassian.com/git/tutorials>
    * branch, merge, rebase
* <http://justinhileman.info/article/changing-history/pee-two.png>
    * rebase peeing down
* <https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work/image-1-big.jpeg>
    * git workflow
* <https://www.sourcetreeapp.com/dam/jcr:580c367b-c240-453d-aa18-c7ced44324f9/hero-mac-screenshot.png?cdnVersion=le>
    * Sourcetree

# Sources
## Literature & Web
* all that are mentioned directly in the presentation
* <https://www.atlassian.com/git/tutorials/what-is-version-control>
* <https://biz30.timedoctor.com/git-mecurial-and-cvs-comparison-of-svn-software/>
