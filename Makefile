.PHONY: all, clean

all:
	docker run --rm -it -v $(PWD):/build gitlab.beilich.de:4567/docker-utilities/pandoc pandoc -H header.tex -f markdown -t beamer presentation.md -o presentation.pdf

clean:
	rm -f *.pdf
